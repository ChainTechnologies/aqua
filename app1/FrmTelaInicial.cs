﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;

namespace app1
{
    public partial class FrmTelaInicial : Form
    {
        public FrmTelaInicial()
        {
            InitializeComponent();
        }

        private void btncalcular_Click(object sender, EventArgs e)
        {

        }

        private void btncancelar_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

        private void btnhistorico_Click(object sender, EventArgs e)
        {
     
        }

        private void caluclularToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmCalcular frmCalcular = new FrmCalcular();
            frmCalcular.TopLevel = false;
            panel1.Controls.Add(frmCalcular);
            frmCalcular.Show();
            Program.menu = false;
         
        }

        private void sairToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void exibirHistóricoToolStripMenuItem_Click(object sender, EventArgs e)
        {
           FrmHistorico frmHistorico = new FrmHistorico();
          // frmHistorico.TopLevel = false;
          // panel1.Controls.Add(frmHistorico);
           frmHistorico.Show();
           Program.menu = false;
          


        }

       

        private void FrmTelaInicial_Load(object sender, EventArgs e)
        {
            Program.menu = true;
        }

        private void cadastrosToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void voltarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void voltarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
    }
}
